/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SERTI;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;


/**
 *
 * @author Usuario
 */
@Entity
@Table(name = "productos_de_tienda_dav")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ProductosDeTiendaDav.findAll", query = "SELECT p FROM ProductosDeTiendaDav p")
    , @NamedQuery(name = "ProductosDeTiendaDav.findByIdProducto", query = "SELECT p FROM ProductosDeTiendaDav p WHERE p.idProducto = :idProducto")
    , @NamedQuery(name = "ProductosDeTiendaDav.findByNombre", query = "SELECT p FROM ProductosDeTiendaDav p WHERE p.nombre = :nombre")
    , @NamedQuery(name = "ProductosDeTiendaDav.findByDescripcion", query = "SELECT p FROM ProductosDeTiendaDav p WHERE p.descripcion = :descripcion")
    , @NamedQuery(name = "ProductosDeTiendaDav.findByPrecio", query = "SELECT p FROM ProductosDeTiendaDav p WHERE p.precio = :precio")})
public class ProductosDeTiendaDav implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Basic(optional = false)
    @Column(name = "id_Producto")
    private Integer idProducto;
    @Size(max = 15)
    @Column(name = "nombre")
    private String nombre;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "descripcion")
    private String descripcion;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "precio")
    private Float precio;

    public ProductosDeTiendaDav() {
    }

    public ProductosDeTiendaDav(Integer idProducto) {
        this.idProducto = idProducto;
    }

    public ProductosDeTiendaDav(Integer idProducto, String descripcion) {
        this.idProducto = idProducto;
        this.descripcion = descripcion;
    }

    public Integer getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(Integer idProducto) {
        this.idProducto = idProducto;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Float getPrecio() {
        return precio;
    }

    public void setPrecio(Float precio) {
        this.precio = precio;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idProducto != null ? idProducto.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ProductosDeTiendaDav)) {
            return false;
        }
        ProductosDeTiendaDav other = (ProductosDeTiendaDav) object;
        if ((this.idProducto == null && other.idProducto != null) || (this.idProducto != null && !this.idProducto.equals(other.idProducto))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "SERTI.ProductosDeTiendaDav[ idProducto=" + idProducto + " ]";
    }
    
}
